﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tetris
{
    class Game
    {
        int SceneWidth = 20;
        int SceneHeight = 20;

        Pixel[,] canvas;

        public Game()
        {
            canvas = new Pixel[SceneWidth, SceneHeight];

            for (int i = 0; i < canvas.GetLength(0); i++)
            {
                for (int j = 0; j < canvas.GetLength(1); j++)
                {
                    canvas[i, j] = new Pixel(i, j);
                }
            }

            UpdateCanvasBorders();
            DrawScene();
        }

        void UpdateCanvasBorders()
        {

            for (int i = 2; i < canvas.GetLength(0) - 2; i++)
            {
                canvas[i, 2].Update('@', ConsoleColor.Gray, true);
                canvas[i, canvas.GetLength(1) - 2].Update('@', ConsoleColor.Gray, true);
            }

            for (int i = 2; i < canvas.GetLength(1) - 1; i++)
            {
                canvas[2, i].Update('@', ConsoleColor.Gray, true);
                canvas[canvas.GetLength(0) - 2, i].Update('@', ConsoleColor.Gray, true);
            }
        }

        void DrawScene()
        {
            for (int i = 0; i < canvas.GetLength(0); i++)
            {
                for (int j = 0; j < canvas.GetLength(1); j++)
                {
                    Print(canvas[i, j]);
                }
            }
        }

        void Print(Pixel pix)
        {
            Console.ForegroundColor = pix.Color;
            Console.SetCursorPosition(pix.X, pix.Y);
            Console.Write(pix.Data);
        }
    }

    class Pixel
    {
        int x;
        int y;
        public int X {
            get { return x; }
        }
        public int Y {
            get { return y; }
        }
        public ConsoleColor Color { get; set; }
        public char Data { get; set; }
        public bool IsBorder { get; set; } = false;

        public Pixel(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void Update(char data, ConsoleColor color, bool isBorder)
        {
            Data = data;
            Color = color;
            IsBorder = isBorder;
        }
    }
}
